## Zoom Marker Library example
A basic example of using zoom marker with Flask to add comments on a plan.

## How to use it

    pip install reqs.txt

    python runserver.py

.. and now open your favorite browser and go to localhost:4000

## Improvements 

Opencv is too heavy to just read files 
