import os
import cv2
from app import app
import urllib.request 
from flask import Flask, flash, request, make_response, redirect, url_for, render_template
from werkzeug.utils import secure_filename
from random import seed
from random import randint


from flask_wtf import FlaskForm
from flask import session

from wtforms import DecimalField, StringField, SubmitField, TextAreaField,\
    SelectField, BooleanField, HiddenField, ValidationError, FloatField, validators

from wtforms.validators import DataRequired


class Task:
  def __init__(self, name, age):
    self.id = id
    self.name = name

class Incident:
    def __init__(self, id, severity, x_coord, y_coord, task_id, comments):
        self.id = id
        self.severity = severity
        self.x_coord = x_coord
        self.y_coord = y_coord
        self.task_id = task_id
        self.comments = comments


class ObsForm(FlaskForm):
    """Form to capture info required for obs handling."""
    x_coord = DecimalField(
        'x', validators=[
            DataRequired()], render_kw={
            'readonly': True})
    y_coord = DecimalField(
        'y', validators=[
            DataRequired()], render_kw={
            'readonly': True})

    comment = TextAreaField('Comments', default='')

@app.route('/')
@app.route('/index')
def index():
    tasks=[]
    for i in range(7):
        t = Task(i, "Tarea %d"%i)
        tasks.append(t)
    print("tasks:",tasks)
    return render_template('plan_view.html')


@app.route('/view_inc')
@app.route('/view_inc/<int:incident_id>')
def view_inc(incident_id=0):
    tasks=[]
    return render_template('plan_view.html', tasks=tasks)

@app.route('/get_obs_json_list')
def get_obs_json_list():

    incidents = []

    for i in range(7):
        inc = Incident(i, randint(0,3), randint(100,1000),randint(100,1000),1,"Prueba %d"%i)
        incidents.append(inc)

    ret_txt = '{ "type": "FeatureCollection", "features": [ '

    i = 1

    for my_incident in incidents:

        ret_txt += '{"geometry": {"type":"Point","coordinates":[%s,%s]}, ' %\
            (my_incident.x_coord,
             my_incident.y_coord)
        ret_txt += '"type": "Feature","properties":{ '
        ret_txt += '"info":"ref. %s",' %\
            (my_incident.comments,)
        ret_txt += '"sev":"%s",' %\
            (my_incident.severity)
        ret_txt += '"tsk":"%s",' %\
            (my_incident.task_id)
        ret_txt += '"dbid" :"%d" }},' % my_incident.id
        i += 1
    # remove last comma and close
    ret_txt = ret_txt[:-1] + ']}'
    #print(ret_txt)
    return (ret_txt)



@app.route('/create_obs')
def create_obs():

    form = ObsForm()

    return render_template('obs_plan_form.html', action="C", obs_type="P", form = form)




"""
Functions to generate images 
"""

@app.route('/get_plan_image')
def get_plan_image():

    basedir = os.path.abspath(os.path.dirname(__file__))
    plan_dir = os.path.join(basedir,"planos")
    image_file_name = os.path.join(plan_dir,"Plano1.jpg")
    img = cv2.imread(image_file_name)
    ret, jpeg = cv2.imencode('.jpg', img)
    
    response = make_response(jpeg.tobytes())
    response.headers['Content-Type'] = 'image/png'
    return response


@app.route('/get_obs_image')
def get_obs_image():

    basedir = os.path.abspath(os.path.dirname(__file__))
    plan_dir = os.path.join(basedir,"imagenes")
    image_file_name = os.path.join(plan_dir,"Imagen1.jpg")
    img = cv2.imread(image_file_name)
    ret, jpeg = cv2.imencode('.jpg', img)
    
    response = make_response(jpeg.tobytes())
    response.headers['Content-Type'] = 'image/png'
    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4000, debug=True)
